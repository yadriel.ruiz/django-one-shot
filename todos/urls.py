from django.urls import path
from todos.views import show_TodoList, show_todolist_detail, create_todolist, update_todolist, delete_todolist, create_todoitem, update_todoitem

urlpatterns = [
    path("items/<int:id>/edit/", update_todoitem, name="todo_item_update"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/", show_todolist_detail, name="todo_list_detail"),
    path("", show_TodoList, name="todo_list_list"),
]