from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from .models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def show_todolist_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id = id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)


def show_TodoList(request):
  todo_list_list = TodoList.objects.all()
  context = {
    "todo_list_list": todo_list_list
  }
  return render(request, "todos/todos.html", context)


def create_todolist(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
      model_instance = form.save()
      return redirect("todo_list_detail", id=model_instance.id)

  else:
    form = TodoListForm()

  context = {
    "form": form
  }

  return render(request, "todos/create.html", context)


def update_todolist(request, id):
    model_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=model_instance)
    context = {
        "form": form,
        "todolist": model_instance
    }
    return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def create_todoitem(request):
    list = TodoList.objects.all()
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
        "list": list,
    }
    return render(request, "todos/items/create.html", context)


def update_todoitem(request, id):
    model_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=model_instance)

    context = {
        "form": form,
        "list": model_instance,
    }
    return render(request, "todos/items/edit.html", context)